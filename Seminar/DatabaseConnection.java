/* @formatter:off
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
@formatter:on */

package Seminar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import com.mysql.cj.jdbc.MysqlDataSource;

public class DatabaseConnection {

    private static String serverName = "127.0.0.1";
    private static int portNumber = 3306;
    private static String databaseName = "classicmodels";
    private static String username = "root";
    private static String password = "panefocaccia.";
    private static String url = "jdbc:mysql://" + serverName + ":" + String.valueOf(portNumber) + "/" + databaseName;

    public static List<HashMap<String, String>> searchEmployee(String lastName) {

        List<HashMap<String, String>> employees = new ArrayList<>();

        Collection<String> retreiveData = Arrays.asList("firstName", "lastName", "employeeNumber", "email", "officeCode", "jobTitle");
        String sql = "SELECT " + String.join(", ", retreiveData) + " FROM employees WHERE lastName = ?";
        try (
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement searchEmployeeData = connection.prepareStatement(sql);
            )
            {
                searchEmployeeData.setString(1, lastName);
            
            try (
                ResultSet resultSet = searchEmployeeData.executeQuery();
            )
            {
                while(resultSet.next()) {
                    HashMap<String, String> employee = new HashMap<>();

                    for (String data : retreiveData) {
                        employee.put(data, resultSet.getString(data));
                    }
                    employees.add(employee);
                }
            }
        }
        catch (SQLException e) {
            System.out.println("Error executing searchEmployeeData query");
            System.out.println(e.getMessage());
        }
        return employees;
    }

    public static int updateEmployeeEmail(int employeeNumber, String email) {

        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setServerName(serverName);
        dataSource.setPortNumber(portNumber);
        dataSource.setDatabaseName(databaseName);
        dataSource.setUser(username);
        dataSource.setPassword(password);

        String sql = "UPDATE employees SET email = ? WHERE employeeNumber = ?";
        int rows_affected = -1;
        
        try (
            Connection connection = dataSource.getConnection();
            PreparedStatement updateEmployeeEmail = connection.prepareStatement(sql);
        )
        {
            updateEmployeeEmail.setString(1, email);
            updateEmployeeEmail.setInt(2, employeeNumber);
            rows_affected = updateEmployeeEmail.executeUpdate();
        }
        catch (SQLException e) {
            System.out.println("Error executing updateEmployeeEmail query");
            System.out.println(e.getMessage());
        }
        return rows_affected;
    }

}
