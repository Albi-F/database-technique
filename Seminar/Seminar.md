# Task 1

### Connecting to MySQL with Powershell
```powershell
mysql -u root -p
```

### Create user
```sql
CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
```

### List users
```sql
SELECT host, user FROM mysql.user;
```

### Grant all privileges
```sql
GRANT ALL PRIVILEGES ON * . * TO 'newuser'@'localhost';
```
The asterisks in this command refer to the database and table (respectively) that they can access — this specific command allows the user to read, edit, execute and perform all tasks across all the databases and tables.

Once you have finalized the permissions that you want to set up for your new users, always be sure to reload all the privileges.
```sql
FLUSH PRIVILEGES;
```

### Common permissions
- ALL PRIVILEGES -> full access to a designated database (or if no database is selected, global access across the system)
- CREATE -> allows them to create new tables or databases
- DROP -> allows them to them to delete tables or databases
- DELETE -> allows them to delete rows from tables
- INSERT -> allows them to insert rows into tables
- SELECT -> allows them to use the SELECT command to read through databases
- UPDATE -> allow them to update table rows
- GRANT OPTION -> allows them to grant or remove other users’ privileges


### Grant a privilege
```sql
GRANT type_of_permission ON database_name.table_name TO 'username'@'localhost';
```

### Revoke a privilege
```sql
REVOKE type_of_permission ON database_name.table_name FROM 'username'@'localhost';
```

### Show user privileges
```sql
SHOW GRANTS FOR 'root'@'localhost';
```

# Task 2

### Display all databases
```sql
SHOW DATABASES
```

### Display all tables of database
```sql
USE database_name;
SHOW TABLES;
```

## Working with backups

### Create backup with mysqldump
```powershell
mysqldump -u root -p database_name | Out-File C:\path\to\backup.sql
```

### Recover database
```powershell
Get-Content C:\path\to\backup.sql | mysql -u root -p database_name
```

When asked for a path and nothing is given, mySQL will use the current folder (the one your terminal is currently in)