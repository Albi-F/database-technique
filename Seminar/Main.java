/* @formatter:off
    _    _ _     _
   / \  | | |__ (_)
  / _ \ | | '_ \| |
 / ___ \| | |_) | |
/_/   \_\_|_.__/|_|
Author: Alberto Fabbri
GitLab: https://gitlab.com/Albi-F/
GitHub: https://github.com/AlbertoFabbri93
@formatter:on */

package Seminar;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {

        String text = "";

        while(!text.equalsIgnoreCase("exit")) {

            System.out.print("\nEnter command: ");

            text = scanner.nextLine();

            switch(text) {

            case "get":
                getEmployeeData();
                break;

            case "update":
                updateEmployee();
                break;

            case "exit":
                System.out.println("Application Closed");
                break;

            default:
                System.out.println("Command not recognized!");
                break;
            }
        }
    }

    public static void getEmployeeData() {

        System.out.print("Insert employee last name: ");
        String lastName = scanner.nextLine();

        List<HashMap<String, String>> employees = DatabaseConnection.searchEmployee(lastName);

        System.out.println("\nFound " + employees.size() + " employees!");

        int i = 0;
        String spacer = " --------------------- ";
        for (HashMap<String, String> employee : employees) {

            System.out.println("\n" + spacer + "Employee " + i++ + spacer);
            employee.forEach((k,v) -> System.out.println(k + ": " + v));

        }
    }

    public static void updateEmployee() {

        System.out.print("Insert employee number: ");
        int employeeNumber = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Insert new email: ");
        String newEmail = scanner.nextLine();

        int rows_affected = DatabaseConnection.updateEmployeeEmail(employeeNumber, newEmail);
        System.out.println("Rows impacted: " + rows_affected);
    }
}
