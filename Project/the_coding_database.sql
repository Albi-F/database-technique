-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: the_coding_database
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `file` (
  `extension` varchar(5) NOT NULL,
  `type` varchar(60) DEFAULT NULL,
  `language_name` varchar(45) NOT NULL,
  PRIMARY KEY (`extension`,`language_name`),
  KEY `fk_file_extension_programming_language1_idx` (`language_name`),
  CONSTRAINT `fk_file_extension_programming_language1` FOREIGN KEY (`language_name`) REFERENCES `language` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
INSERT INTO `file` VALUES ('c','source code','C'),('cc','source code','C++'),('class','compiled java code','Java'),('cpp','source code','C++'),('h','header file','C'),('hs','source code','Haskell'),('inc','include file','Pascal'),('jar','Java archive used by the JRE','Java'),('java','source code','Java'),('pas','source code','Pascal'),('py','source code','Python'),('pyc','intermediate representation','Python'),('pyd','dynamic-link library Microsoft Windows','Python'),('pyo','intermediate representation','Python'),('rb','source code','Ruby'),('sc','worksheet','Scala'),('scala','source code','Scala');
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `language` (
  `name` varchar(45) NOT NULL,
  `origin` varchar(45) DEFAULT NULL,
  `website` varchar(70) DEFAULT NULL,
  `media_type` varchar(45) DEFAULT NULL,
  `license_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `media_type_UNIQUE` (`media_type`),
  KEY `fk_programming_language_license1_idx` (`license_name`),
  CONSTRAINT `fk_programming_language_license1` FOREIGN KEY (`license_name`) REFERENCES `license` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES ('C','derived from an earlier language called \'B\'',NULL,NULL,'GNU General Public License'),('C++','OOP derviative of C','https://isocpp.org/','text/x-c','GNU General Public License'),('Cobol','acronym for COmmon Business-Oriented Language',NULL,NULL,NULL),('Go',NULL,'https://golang.org',' text/x-go','3-clause BSD License'),('Haskell','Haskell Curry',' https://www.haskell.org',NULL,'3-clause BSD License'),('Java','Java coffee','https://www.java.com','text/x-java-source','GNU General Public License'),('JavaScript','use the reputation of Java',NULL,'application/javascript','Mozilla Public License'),('Kotlin','Kotlin Island','https://kotlinlang.org/','text/x-kotlin','Apache License'),('Lua','Moon','https://www.lua.org/','text/x-lua','MIT license'),('Pascal','Blaise Pascal',NULL,'text/x-pascal','GNU General Public License'),('Perl',NULL,' https://www.perl.org','application/x-perl','GNU General Public License'),('PHP',NULL,'https://www.php.net','application/x-httpd-php','PHP License'),('Python','Monty Python','https://www.python.org/','text/x-python','Python Software Foundation License'),('Ruby','The ruby gemstone','https://www.ruby-lang.org',' application/x-ruby','2-clause BSD License'),('Rust','Pucciniales','https://www.rust-lang.org','text/x-rust','MIT license'),('Scala','a portmanteau of scalable and language','https://www.scala-lang.org/',NULL,'Apache License'),('Swift',NULL,'https://swift.org',NULL,'Apache License');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_has_paradigm`
--

DROP TABLE IF EXISTS `language_has_paradigm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `language_has_paradigm` (
  `language_name` varchar(45) NOT NULL,
  `paradigm_name` varchar(45) NOT NULL,
  PRIMARY KEY (`language_name`,`paradigm_name`),
  KEY `fk_programming_language_has_programming_paradigm_programmin_idx` (`paradigm_name`),
  KEY `fk_programming_language_has_programming_paradigm_programmin_idx1` (`language_name`),
  CONSTRAINT `fk_programming_language_has_programming_paradigm_programming_1` FOREIGN KEY (`language_name`) REFERENCES `language` (`name`),
  CONSTRAINT `fk_programming_language_has_programming_paradigm_programming_2` FOREIGN KEY (`paradigm_name`) REFERENCES `paradigm` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_has_paradigm`
--

LOCK TABLES `language_has_paradigm` WRITE;
/*!40000 ALTER TABLE `language_has_paradigm` DISABLE KEYS */;
INSERT INTO `language_has_paradigm` VALUES ('C++','functional programming'),('Go','functional programming'),('Haskell','functional programming'),('Java','functional programming'),('Lua','functional programming'),('Python','functional programming'),('Scala','functional programming'),('C++','generic programming'),('Java','generic programming'),('C','imperative programming'),('Go','imperative programming'),('Java','imperative programming'),('Lua','imperative programming'),('Python','imperative programming'),('Scala','imperative programming'),('C++','object-oriented programming'),('Go','object-oriented programming'),('Java','object-oriented programming'),('Lua','object-oriented programming'),('Python','object-oriented programming'),('Scala','object-oriented programming'),('C++','procedural programming'),('Go','procedural programming'),('Lua','procedural programming'),('Lua','prototype-based programming'),('Go','structured programming'),('Java','structured programming');
/*!40000 ALTER TABLE `language_has_paradigm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_is_influenced_by_language`
--

DROP TABLE IF EXISTS `language_is_influenced_by_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `language_is_influenced_by_language` (
  `language_influenced` varchar(45) NOT NULL,
  `influential_language` varchar(45) NOT NULL,
  PRIMARY KEY (`language_influenced`,`influential_language`),
  KEY `fk_programming_language_has_programming_language_programmin_idx` (`influential_language`),
  KEY `fk_programming_language_has_programming_language_programmin_idx1` (`language_influenced`),
  CONSTRAINT `fk_programming_language_has_programming_language_programming_1` FOREIGN KEY (`language_influenced`) REFERENCES `language` (`name`),
  CONSTRAINT `fk_programming_language_has_programming_language_programming_2` FOREIGN KEY (`influential_language`) REFERENCES `language` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_is_influenced_by_language`
--

LOCK TABLES `language_is_influenced_by_language` WRITE;
/*!40000 ALTER TABLE `language_is_influenced_by_language` DISABLE KEYS */;
INSERT INTO `language_is_influenced_by_language` VALUES ('C++','C'),('Java','C'),('Perl','C'),('Python','C'),('Java','C++'),('Lua','C++'),('Perl','C++'),('Python','C++'),('Ruby','C++'),('Rust','C++'),('Python','Haskell'),('Rust','Haskell'),('Swift','Haskell'),('Python','Java'),('Ruby','Lua'),('Python','Perl'),('Ruby','Perl'),('Ruby','Python'),('Swift','Python'),('Rust','Ruby'),('Swift','Ruby'),('Swift','Rust'),('Rust','Swift');
/*!40000 ALTER TABLE `language_is_influenced_by_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `license`
--

DROP TABLE IF EXISTS `license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `license` (
  `name` varchar(50) NOT NULL,
  `acronym` varchar(10) DEFAULT NULL,
  `website` varchar(80) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `publication_date` year DEFAULT NULL,
  `osi_approved` tinyint NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `license`
--

LOCK TABLES `license` WRITE;
/*!40000 ALTER TABLE `license` DISABLE KEYS */;
INSERT INTO `license` VALUES ('2-clause BSD License',NULL,NULL,'The FreeBSD Project',1999,1),('3-clause BSD License','BSD-3',NULL,'Regents of the University of California',1999,1),('Apache License',NULL,'https://www.apache.org/licenses/',' Apache Software Foundation',1990,1),('Code Project Open License','CPOL','www.codeproject.com/info/cpol10.aspx','The Code Project',2008,0),('Common Public Attribution License','CPAL',NULL,'Socialtext',2007,1),('Eclipse Public License','EPL','https://eclipse.org/legal/eplfaq.php','Eclipse Foundation',2004,1),('European Union Public Licence','EUPL','	joinup.ec.europa.eu/page/eupl-guidelines-faq-infographics','European Commission',2007,1),('GNU Affero General Public License','GNU AGPL','https://gnu.org/licenses/agpl.html','Free Software Foundation',2007,1),('GNU General Public License','GNU GPL','https://www.gnu.org/licenses/gpl.html','Free Software Foundation',1989,1),('GNU Lesser General Public License','GNU LGPL','https://gnu.org/licenses/lgpl.html','Free Software Foundation',1991,1),('MIT license',NULL,NULL,'Massachusetts Institute of Technology',1984,1),('Mozilla Public License','MPL','www.mozilla.org/MPL','Mozilla Foundation',2012,1),('PHP License',NULL,'https://secure.php.net/license','PHP Group',NULL,1),('Python Software Foundation License','PSFL','https://www.python.org/psf/license/','Python Software Foundation',2001,1),('Server Side Public License','SSPL','https://www.mongodb.com/licensing/server-side-public-license','MongoDB Inc.',2018,0);
/*!40000 ALTER TABLE `license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paradigm`
--

DROP TABLE IF EXISTS `paradigm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paradigm` (
  `name` varchar(45) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paradigm`
--

LOCK TABLES `paradigm` WRITE;
/*!40000 ALTER TABLE `paradigm` DISABLE KEYS */;
INSERT INTO `paradigm` VALUES ('declarative programming','expresses the logic of a computation without describing its control flow'),('functional programming','programs are constructed by applying and composing functions'),('generic programming','way of designing and writing programs where algorithms are written in terms of parametric types'),('imperative programming','programming paradigm that uses statements that change the state of the program'),('object-oriented programming','programming paradigm based on the concept of objects'),('procedural programming','programming paradigm based on the concept of the procedure call'),('prototype-based programming','programming paradigm in which behaviour reuse (known as inheritance) is performed via a process of reusing existing objects that serve as prototypes'),('structured programming','programming paradigm aimed at improving clarity, quality, and development time by using control structures');
/*!40000 ALTER TABLE `paradigm` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-07 19:23:41
