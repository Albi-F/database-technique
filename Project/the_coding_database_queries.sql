/*
 *     _    _ _     _
 *    / \  | | |__ (_)
 *   / _ \ | | '_ \| |
 *  / ___ \| | |_) | |
 * /_/   \_\_|_.__/|_|
 * Author: Alberto Fabbri
 * GitLab: https://gitlab.com/Albi-F/
 * GitHub: https://github.com/AlbertoFabbri93
 */
-- programming_language table
DELETE FROM
  language;
INSERT INTO
  language (name, origin, website, media_type, license_name)
VALUES
  (
    "Java",
    "Java coffee",
    "https://www.java.com",
    "text/x-java-source",
    "GNU General Public License"
  ),
  (
    "Python",
    "Monty Python",
    "https://www.python.org/",
    "text/x-python",
    "Python Software Foundation License"
  ),
  (
    "Scala",
    "a portmanteau of scalable and language",
    "https://www.scala-lang.org/",
    NULL,
    "Apache License"
  ),
  (
    "C",
    "derived from an earlier language called 'B'",
    NULL,
    NULL,
    "GNU General Public License"
  ),
  (
    "JavaScript",
    "Java",
    NULL,
    "application/javascript",
    "Mozilla Public License"
  ),
  (
    "Rust",
    "Pucciniales",
    "https://www.rust-lang.org",
    "text/x-rust",
    "MIT license"
  ),
  (
    "Haskell",
    "Haskell Curry",
    " https://www.haskell.org",
    NULL,
    "3-clause BSD License"
  ),
  (
    "Go",
    NULL,
    "https://golang.org",
    " text/x-go",
    "3-clause BSD License"
  ),
  (
    "Pascal",
    "Blaise Pascal",
    NULL,
    "text/x-pascal",
    "GNU General Public License"
  ),
  (
    "Ruby",
    "The ruby gemstone",
    "https://www.ruby-lang.org",
    " application/x-ruby",
    "2-clause BSD License"
  ),
  (
    "C++",
    "OOP derviative of C",
    "https://isocpp.org/",
    "text/x-c",
    "GNU General Public License"
  ),
  (
    "Lua",
    "Moon",
    "https://www.lua.org/",
    "text/x-lua",
    "MIT license"
  ),
  (
    "Perl",
    NULL,
    " https://www.perl.org",
    "application/x-perl",
    "GNU General Public License"
  ),
  (
    "Cobol",
    "acronym for COmmon Business-Oriented Language",
    NULL,
    NULL,
    NULL
  ),
  (
    "Kotlin",
    "Kotlin Island",
    "https://kotlinlang.org/",
    "text/x-kotlin",
    "Apache License"
  ),
  (
    "Swift",
    NULL,
    "https://swift.org",
    NULL,
    "Apache License"
  ),
  (
    "PHP",
    NULL,
    "https://www.php.net",
    "application/x-httpd-php",
    "PHP License"
  );
SELECT
  *
FROM
  language;
-- programming_paradigm table
DELETE FROM
  paradigm;
INSERT INTO
  paradigm (name, description)
VALUES
  (
    "imperative programming",
    "programming paradigm that uses statements that change the state of the program"
  ),
  (
    "declarative programming",
    "expresses the logic of a computation without describing its control flow"
  ),
  (
    "object-oriented programming",
    "programming paradigm based on the concept of objects"
  ),
  (
    "generic programming",
    "way of designing and writing programs where algorithms are written in terms of parametric types"
  ),
  (
    "functional programming",
    "programs are constructed by applying and composing functions"
  ),
  (
    "structured programming",
    "programming paradigm aimed at improving clarity, quality, and development time by using control structures"
  ),
  (
    "procedural programming",
    "programming paradigm based on the concept of the procedure call"
  ),
  (
    "prototype-based programming",
    "programming paradigm in which behaviour reuse (known as inheritance) is performed via a process of reusing existing objects that serve as prototypes"
  );
SELECT
  *
FROM
  paradigm;
-- license
DELETE FROM
  license;
INSERT INTO
  license (
    name,
    acronym,
    website,
    author,
    publication_date,
    osi_approved
  )
VALUES
  (
    "GNU Lesser General Public License",
    "GNU LGPL",
    "https://gnu.org/licenses/lgpl.html",
    "Free Software Foundation",
    "1991",
    TRUE
  ),
  (
    "GNU General Public License",
    "GNU GPL",
    "https://www.gnu.org/licenses/gpl.html",
    "Free Software Foundation",
    "1989",
    TRUE
  ),
  (
    "MIT license",
    NULL,
    NULL,
    "Massachusetts Institute of Technology",
    "1984",
    TRUE
  ),
  (
    "Apache License",
    NULL,
    "https://www.apache.org/licenses/",
    " Apache Software Foundation",
    "1990",
    TRUE
  ),
  (
    "Eclipse Public License",
    "EPL",
    "https://eclipse.org/legal/eplfaq.php",
    "Eclipse Foundation",
    "2004",
    TRUE
  ),
  (
    "Server Side Public License",
    "SSPL",
    "https://www.mongodb.com/licensing/server-side-public-license",
    "MongoDB Inc.",
    "2018",
    FALSE
  ),
  (
    "GNU Affero General Public License",
    "GNU AGPL",
    "https://gnu.org/licenses/agpl.html",
    "Free Software Foundation",
    "2007",
    TRUE
  ),
  (
    "Common Public Attribution License",
    "CPAL",
    NULL,
    "Socialtext",
    "2007",
    TRUE
  ),
  (
    "Mozilla Public License",
    "MPL",
    "www.mozilla.org/MPL",
    "Mozilla Foundation",
    "2012",
    TRUE
  ),
  (
    "European Union Public Licence",
    "EUPL",
    "	joinup.ec.europa.eu/page/eupl-guidelines-faq-infographics",
    "European Commission",
    "2007",
    TRUE
  ),
  (
    "Code Project Open License",
    "CPOL",
    "www.codeproject.com/info/cpol10.aspx",
    "The Code Project",
    "2008",
    FALSE
  ),
  (
    "Python Software Foundation License",
    "PSFL",
    "https://www.python.org/psf/license/",
    "Python Software Foundation",
    "2001",
    TRUE
  ),
  (
    "3-clause BSD License",
    "BSD-3",
    NULL,
    "Regents of the University of California",
    "1999",
    TRUE
  ),
  (
    "2-clause BSD License",
    NULL,
    NULL,
    "The FreeBSD Project",
    "1999",
    TRUE
  ),
  (
    "PHP License",
    NULL,
    "https://secure.php.net/license",
    "PHP Group",
    NULL,
    TRUE
  );
SELECT
  *
FROM
  license;
-- programming_language_has_programming_paradigm
DELETE FROM
  language_has_paradigm;
INSERT INTO
  language_has_paradigm (language_name, paradigm_name)
VALUES
  ("C", "imperative programming"),
  ("Java", "structured programming"),
  ("Java", "object-oriented programming"),
  ("Java", "imperative programming"),
  ("Java", "functional programming"),
  ("Python", "functional programming"),
  ("Python", "object-oriented programming"),
  ("Python", "imperative programming"),
  ("Java", "generic programming"),
  ("Scala", "functional programming"),
  ("Scala", "imperative programming"),
  ("Scala", "object-oriented programming"),
  ("Go", "structured programming"),
  ("Go", "procedural programming"),
  ("Go", "imperative programming"),
  ("Go", "object-oriented programming"),
  ("Go", "functional programming"),
  ("Haskell", "functional programming"),
  ("C++", "object-oriented programming"),
  ("C++", "functional programming"),
  ("C++", "procedural programming"),
  ("C++", "generic programming"),
  ("Lua", "object-oriented programming"),
  ("Lua", "functional programming"),
  ("Lua", "procedural programming"),
  ("Lua", "prototype-based programming"),
  ("Lua", "imperative programming");
SELECT
  *
FROM
  language_has_paradigm;
-- programming_language_is_influenced_by_programming_language
DELETE FROM
  language_is_influenced_by_language;
INSERT INTO
  language_is_influenced_by_language (language_influenced, influential_language)
VALUES
  ("Python", "C"),
  ("Python", "C++"),
  ("Python", "Perl"),
  ("Python", "Java"),
  ("Python", "Haskell"),
  ("C++", "C"),
  ("Lua", "C++"),
  ("Perl", "C"),
  ("Perl", "C++"),
  ("Java", "C"),
  ("Java", "C++"),
  ("Ruby", "Perl"),
  ("Ruby", "Python"),
  ("Ruby", "C++"),
  ("Ruby", "Lua"),
  ("Swift", "Rust"),
  ("Swift", "Haskell"),
  ("Swift", "Ruby"),
  ("Swift", "Python"),
  ("Rust", "C++"),
  ("Rust", "Haskell"),
  ("Rust", "Ruby"),
  ("Rust", "Swift");
SELECT
  *
FROM
  language_is_influenced_by_language;
-- file_extension
DELETE FROM
  file;
INSERT INTO
  file (language_name, extension, type)
VALUES
  ("Python", "py", "source code"),
  ("Python", "pyc", "intermediate representation"),
  (
    "Python",
    "pyd",
    "dynamic-link library Microsoft Windows"
  ),
  ("Python", "pyo", "intermediate representation"),
  ("C", "c", "source code"),
  ("C", "h", "header file"),
  ("C++", "cc", "source code"),
  ("C++", "cpp", "source code"),
  ("Pascal", "pas", "source code"),
  ("Pascal", "inc", "include file"),
  ("Scala", "scala", "source code"),
  ("Scala", "sc", "worksheet"),
  ("Ruby", "rb", "source code"),
  ("Java", "java", "source code"),
  ("Java", "jar", "Java archive used by the JRE"),
  ("Java", "class", "compiled java code");
SELECT
  *
FROM
  file;
-- query 1 --
SELECT
  *
FROM
  language
WHERE
  license_name = "GNU General Public License"
ORDER BY
  name DESC;
-- query 2 --
SELECT
  COUNT(*) AS "languages_influenced_by_C++"
FROM
  language_is_influenced_by_language
WHERE
  influential_language = 'C++';
-- query 3 --
SELECT
  COUNT(language_name) AS "languages use paradigm",
  paradigm_name
FROM
  language_has_paradigm
GROUP BY
  paradigm_name;
-- query 4 --
INSERT INTO
  file (language_name, extension, type) VALUE ("Haskell", "hs", "source code");
-- query 5 --
DELETE FROM
  file
WHERE
  language_name = "Haskell";
-- query 6 --
SELECT
  language_has_paradigm.language_name,
  language_has_paradigm.paradigm_name,
  paradigm.description
FROM
  language_has_paradigm
  JOIN paradigm ON language_has_paradigm.paradigm_name = paradigm.name
WHERE
  language_name = "Java";
-- query 7 --
SELECT
  license.name,
  COUNT(*) AS "languages_has_license"
FROM
  license
  JOIN language ON license.name = language.license_name
GROUP BY
  license.name;
-- query 8 --
SELECT
  file.language_name,
  file.extension,
  license.osi_approved
FROM
  file
  INNER JOIN language ON file.language_name = language.name
  INNER JOIN license ON language.license_name = license.name;
-- query 9 --
SELECT
  *
FROM
  license
WHERE
  publication_date BETWEEN 1990
  AND 2000;
-- query 10 --
SELECT
  website
FROM
  language
UNION
SELECT
  website
FROM
  license;
-- query 11 --
UPDATE language SET origin = "use the reputation of Java" WHERE name = "JavaScript";
-- reverse query 11
UPDATE language SET origin = "Java" WHERE name = "JavaScript";