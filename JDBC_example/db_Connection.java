package JDBC_example;

import java.sql.*;

public class db_Connection {

    private static String username = "root";
    private static String password = "panefocaccia.";
    private String url = "jdbc:mysql://127.0.0.1:3306/world";
    
    Connection connection;
    Statement statement;
    ResultSet resultSet;

    public db_Connection() {

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            System.out.println("Connection failed!");
        }

    }

    public void searchForCountry(String code) {

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT name FROM Country WHERE code ='" + code + "'");

            // Read the result from resultSet
            while(resultSet.next()) {
                System.out.println(code + "-->" + resultSet.getString("name"));
            }

        } catch(SQLException ex) {
            System.out.println("Error executing query");
        }

    }

    public void disconnect() {

        try{
            if (connection != null) {
                connection.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
        } catch(SQLException ex) {
            System.out.println("Failed to disconnect!");
        }
    }

}